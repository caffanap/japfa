import {
  Component
} from '@angular/core';
import {
  NavController,
  NavParams,
  AlertController,
  LoadingController
} from 'ionic-angular';
import {
  Http
} from '@angular/http';
import {
  Storage
} from '@ionic/storage';

/**
 * Generated class for the KirimemailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-kirimemail',
  templateUrl: 'kirimemail.html',
})
export class KirimemailPage {


  emailnya = [];
  getEmail = [];
  url_base: String = 'https://jfapps.japfafoundation.org/';
  crl_galeri : String = 'https://jfapps.japfafoundation.org/assets/img/galeri';
  inBalas: string = '';
  inJudul: string = '';
  emailsaya: string = '';

  constructor(public storage: Storage, public loading: LoadingController, public http: Http, public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {
    this.storage.get("info_user").then(val => {
      this.emailsaya = val.email
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad KirimemailPage');
    console.log(this.emailsaya);

  }

  // ambilemailInternet(inputan) {
  //   this.http.get(this.url_base + 'mobile/data_email?query=' + inputan)
  //     .map(respon => respon.json())
  //     .subscribe((val) => {
  //       console.log(val);
  //       val.forEach(data => {
  //         this.getEmail = [data.id];
  //       });
  //     });

  // }

  kirim() {



    if (this.inBalas == '') {
      let alert = this.alertCtrl.create({
        title: "Peringatan!",
        subTitle: "Data tidak boleh kosong",
        buttons: ['OK']
      });
      alert.present();
    } else {
      let loding = this.loading.create({
        content: "Harap Tunggu..."
      });
      loding.present();
        
        this.http.get(this.url_base + 'mobile/mobile_add_mailbox?judul=' + this.inJudul + '&pengirim=' + this.emailsaya + '&penerima=support@japfafoundation.org&isi=' + this.inBalas)
          .map(respon => respon.json())
          .subscribe((val) => {
            loding.dismiss();
            let alert = this.alertCtrl.create({
              title: "Berhasil!",
              subTitle: "Pesan anda telah dikirim",
              buttons: ['OK']
            });
            alert.present();
            console.log(val);
            this.navCtrl.popTo(KirimemailPage);
          });

    }


  }

}
