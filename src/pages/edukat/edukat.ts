import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { SdevelopPage } from '../sdevelop/sdevelop';
import { KotaPage } from '../kota/kota';
import { BencanadetailPage } from '../bencanadetail/bencanadetail';
import { NutdetPage } from '../nutdet/nutdet';
import { SportdetPage } from '../sportdet/sportdet';
/**
 * Generated class for the EdukatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-edukat',
  templateUrl: 'edukat.html',
})
export class EdukatPage {

  bahasa: string;
  id: Number;
  kat: string;
  judul: String = "";
  url_base: String = 'https://jfapps.japfafoundation.org/';
  crl_galeri : String = 'https://jfapps.japfafoundation.org/assets/img/galeri';
  dataList: any;

  constructor(public storage: Storage, public http: Http, public loading: LoadingController, public navCtrl: NavController, public navParams: NavParams) {
    this.id = this.navParams.get("idnya");
    this.judul = this.navParams.get("judul");
    this.kat = this.navParams.get("kat");
    this.storage.get("bahasa").then(val => {
      this.bahasa = val
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EdukatPage');
    let loding = this.loading.create({
      content: "Harap Tunggu..."
    }); 
 
    loding.present();
    this.http.get(this.url_base+'api_kat/view_data_det/'+this.id+'/'+this.kat)
    .map(respon => respon.json())
      .subscribe((val) => {
          loding.dismiss();
          console.log(val);
          this.dataList = val;

      });
  }

  develop(id, nama){
    console.log(id);
    if (this.kat == "edu") {
      this.navCtrl.push(SdevelopPage, {idnya: id, judul: nama});
    }else if(this.kat == "soci"){
      this.navCtrl.push(KotaPage, {idnya: id, judul: nama});
    }else if(this.kat == "disas"){
      this.navCtrl.push(BencanadetailPage, {idnya: id, judul: nama});
    }else if(this.kat == "nut"){
      this.navCtrl.push(NutdetPage, {idnya: id, judul: nama});
    }else if(this.kat == "sport"){
      this.navCtrl.push(SportdetPage, {idnya: id, judul: nama});
    }
  }

}
