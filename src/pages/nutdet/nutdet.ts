import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { SmadetailPage } from '../smadetail/smadetail';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'page-nutdet',
  templateUrl: 'nutdet.html',
})
export class NutdetPage {

  url_base: String = 'https://jfapps.japfafoundation.org/';
  crl_galeri : String = 'https://jfapps.japfafoundation.org/assets/img/galeri';
  judul: String = "";
  id: Number;
  tampung_nutdet_awal: any;
  tampung_nutdet: any;
  nama_nutdetdet: string;
  pelaksana: string;
  tujuan: string;
  bahasa: string;

  constructor(public storage: Storage,public http: Http, public loading: LoadingController,public navCtrl: NavController, public navParams: NavParams) {
    this.id = this.navParams.get("idnya");
    this.judul = this.navParams.get("judul");
    this.storage.get("bahasa").then(val => {
      this.bahasa = val
    });
  }

  ionViewDidLoad() {
    let loding = this.loading.create({
      content: "Harap Tunggu..."
    });

    loding.present();
    this.http.get(this.url_base+'detgizi/mobile_view_atas?id='+this.id)
    .map(respon => respon.json())
      .subscribe((val) => {
          console.log(val);
          this.tampung_nutdet_awal = val;

      });

      this.storage.get("bahasa").then(val => {
        this.http.get(this.url_base+'detgizi/mobile_view?id='+this.id+'&bahasa='+val)
        .map(respon => respon.json())
          .subscribe((val) => {
            loding.dismiss();
              console.log(val);
              this.tampung_nutdet = val;
    
          });
      });
  }


  smadetail(id_nutdet, id_detgizi, lat, long, nama_nutdetdet_indo,nama_nutdetdet_ing, pelaksana_indo,pelaksana_ing, status, tgl_akhir, tgl_mulai, tgl_reg, tujuan_indo,tujuan_ing, gambar, alamat){
    
    this.storage.get("bahasa").then(val => {
      if (val == "ing") {
        this.pelaksana = pelaksana_ing
        this.tujuan = tujuan_ing
        this.nama_nutdetdet = nama_nutdetdet_ing
      }else if (val == "indo") {
        this.pelaksana = pelaksana_indo
        this.tujuan = tujuan_indo
        this.nama_nutdetdet = nama_nutdetdet_indo
      }

      let data  = [
      {
        id_edunya: id_nutdet,
        id_detedunya: id_detgizi,
        latnya: lat,
        longnya: long,
        nama_edudetnya: this.nama_nutdetdet,
        pelaksananya: this.pelaksana,
        statusnya: status,
        tgl_akhirnya: tgl_akhir,
        tgl_mulainya: tgl_mulai,
        tgl_regnya: tgl_reg,
        tujuannya: this.tujuan,
        gambarnya: gambar,
        alamatnya: alamat,
        kategori: "cosi"
      }
    ];
    this.storage.set("data_detail", data);
    this.navCtrl.push(SmadetailPage,{ parameternya: data });

    });
    
    

  }


}
