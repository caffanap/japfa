import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';

import { Http } from '@angular/http';

/**
 * Generated class for the DetailmailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-detailmail',
  templateUrl: 'detailmail.html',
})
export class DetailmailPage {

  id_mailboxnya: String = '';
  judul_mailboxnya: String = '';
  penerima_mailboxnya: String = '';
  pengirim_mailboxnya: String = '';
  tanggal_mailboxnya: String = '';
  isi_mailboxnya: String = '';
  gambarnya: String = '';
  url_base : String = 'https://japfafoundation.org/public/';
  crl_galeri : String = 'http://japfafoundation.org/public/assets/img/user/';
  balasan: Boolean = false;
  inBalas: String = '';

  constructor(public loading: LoadingController, public http: Http, public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {
    const a = this.navParams.get("parameternya");
    this.id_mailboxnya = a.id_mailboxnya;
    this.judul_mailboxnya = a.judul_mailboxnya;
    this.penerima_mailboxnya = a.penerima_mailboxnya;
    this.pengirim_mailboxnya = a.pengirim_mailboxnya;
    this.tanggal_mailboxnya = a.tanggal_mailboxnya;
    this.gambarnya = a.gambarnya;
    this.isi_mailboxnya = a.isi_mailboxnya;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailmailPage');
  }

  balasPesan(){
    this.balasan = !this.balasan;
  }

  resend(){
    if (this.inBalas == '') {
      let alert = this.alertCtrl.create({
        title: "Peringatan!",
        subTitle: "Data tidak boleh kosong",
        buttons: ['OK']
      });
      alert.present(); 
    }else{
      let loding = this.loading.create({
        content: "Harap Tunggu..."
      });
      loding.present();
      this.http.get(this.url_base+'mobile/mobile_add_mailbox?judul='+this.judul_mailboxnya+'&pengirim='+this.penerima_mailboxnya+'&penerima='+this.pengirim_mailboxnya+'&isi='+this.inBalas)
      .map(respon => respon.json())
        .subscribe((val) => {
            loding.dismiss();
            let alert = this.alertCtrl.create({
              title: "Berhasil!",
              subTitle: "Pesan anda telah dilanjutkan ke "+this.pengirim_mailboxnya,
              buttons: ['OK']
            });
            alert.present(); 
            console.log(val);
            this.inBalas = "";
            this.balasan = false;
        });
    }
  }

}
