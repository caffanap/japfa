import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { DetailmailPage } from '../detailmail/detailmail';
import { KirimemailPage } from '../kirimemail/kirimemail';

/**
 * Generated class for the MailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-mail',
  templateUrl: 'mail.html',
})
export class MailPage {

  @ViewChild('searchBar') myInput;

  isSearchbarOpened: Boolean;

  judul: String = "";
  tabnya: String;
  url_base : String = 'https://japfafoundation.org/public/';
  crl_galeri : String = 'http://japfafoundation.org/public/assets/img/user/';
  dataList: any;
  datanya: any;
  dataListOut: any;
  datanyaOut: any;
  jumah_inbox: Number = 0;
  jumah_outbox: Number = 0;
  constructor(public http: Http, public loading: LoadingController, public storage: Storage, public navCtrl: NavController, public navParams: NavParams) {
    
    this.tabnya = "inbox";
  }

  kirimEmail(){

    this.navCtrl.push(KirimemailPage, { pengirim : this.judul});
  }

  ionViewDidEnter() {
    this.storage.get("info_user").then(val => {
      this.judul = val.email;
      let loding = this.loading.create({
        content: "Harap Tunggu..."
      });
  
      loding.present();
      this.http.get(this.url_base+'mobile/user_mailbox?id='+val.id_user)
      .map(respon => respon.json())
        .subscribe((val) => {
            this.jumah_inbox = val[0].jumlah_inbox;
            this.jumah_outbox = val[0].jumlah_outbox;
            
        });

    this.http.get(this.url_base+'mobile/mailbox?email='+val.email+'&aksi=inbox')
      .map(respon => respon.json())
        .subscribe((val) => {
            console.log(val);
            this.dataList = val;
            this.datanya = val;
        });


        this.http.get(this.url_base+'mobile/mailbox?email='+val.email+'&aksi=outbox')
      .map(respon => respon.json())
        .subscribe((val) => {
            console.log(val);
            this.dataListOut = val;
            this.datanyaOut = val;
            loding.dismiss();
        });



      });

      
    

  }

  dataSementara(){
    this.datanya = this.dataList;
    this.datanyaOut = this.dataListOut;
  }

  detailmail(id_mailbox, judul_mailbox, penerima_mailbox, pengirim_mailbox, tanggal_mailbox, gambar, isi_mailbox){
    const param = {
      id_mailboxnya: id_mailbox,
      judul_mailboxnya: judul_mailbox,
      penerima_mailboxnya: penerima_mailbox,
      pengirim_mailboxnya: pengirim_mailbox,
      tanggal_mailboxnya: tanggal_mailbox,
      gambarnya: gambar,
      isi_mailboxnya: isi_mailbox 
    }
    this.navCtrl.push(DetailmailPage, {parameternya: param});
  }


  togglenya(){
    this.isSearchbarOpened=true;
    setTimeout(() => {
      this.myInput.setFocus();
    },300);
  }

  onSearch(event: any){

    this.dataSementara();

    const val = event.target.value;

    if (this.tabnya == "inbox") {
      if (val && val.trim() != '') {
        this.datanya = this.datanya.filter((item) => {
          return (item.judul_mailbox.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
      }
    }else{
      if (val && val.trim() != '') {
        this.datanyaOut = this.datanyaOut.filter((item) => {
          return (item.judul_mailbox.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
      }
    }

    
    

  }

}
