import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { EdukatPage } from '../edukat/edukat';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the SportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-sport',
  templateUrl: 'sport.html',
})
export class SportPage {

  sport: any;
  bahasa: string;
  url_base: String = 'https://jfapps.japfafoundation.org/';
  crl_galeri : String = 'https://jfapps.japfafoundation.org/assets/img/galeri';
  nama_sportdet: string = "";
  pelaksana: string = "";
  tujuan: string = "";

  constructor(public storage: Storage, public http: Http, public loading: LoadingController,public navCtrl: NavController, public navParams: NavParams) {

    this.storage.get("bahasa").then(val => {
      this.bahasa = val
      
    });



  }

  ionViewDidLoad() {
    let loding = this.loading.create({
      content: "Harap Tunggu..."
    });

    loding.present();
    this.http.get(this.url_base+'sport/mobile_view')
    .map(respon => respon.json())
      .subscribe((val) => {
          loding.dismiss();
          console.log(val);
          this.sport = val;

      });

  }

  kota(id_sport, nama){
    console.log(id_sport);
    this.navCtrl.push(EdukatPage, {idnya: id_sport, judul: nama, kat: "sport"});
  }



  // smadetail(id_sport){
  //   this.http.get(this.url_base+'mobile/sport?id='+id_sport+'&bahasa='+this.bahasa)
  //   .map(respon => respon.json())
  //     .subscribe((val) => { 
  //       console.log(val[0]);

            
  //             if (this.bahasa == "ing") {
  //               this.nama_sportdet = val[0].nama_sportdet_ing;
  //               this.pelaksana = val[0].pelaksana_ing;
  //               this.tujuan = val[0].tujuan_ing;
  //             }else if (this.bahasa == "indo") {
  //               this.nama_sportdet = val[0].nama_sportdet;
  //               this.pelaksana = val[0].pelaksana;
  //               this.tujuan = val[0].tujuan;
  //             }
            
  //           let dataparam = [{
  //             id_edunya: val[0].id_detsport,
  //             id_detedunya: val[0].id_sport,
  //             latnya: val[0].lat,
  //             longnya: val[0].long,
  //             nama_edudetnya: this.nama_sportdet,
  //             pelaksananya: this.pelaksana,
  //             statusnya: val[0].status,
  //             tgl_akhirnya: val[0].tgl_akhir,
  //             tgl_mulainya: val[0].tgl_mulai,
  //             tgl_regnya: val[0].tgl_reg,
  //             tujuannya: this.tujuan,
  //             gambarnya: val[0].gambar,
  //             alamatnya: val[0].alamat,
  //             kategori: "sport"
  //           }];

  //           this.storage.set("data_detail", dataparam);
  //           console.log(dataparam);
  //           this.navCtrl.push(SmadetailPage, {parameternya: dataparam});

 
  //     });
  // }

}
