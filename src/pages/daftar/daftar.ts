import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, Events} from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';

/**
 * Generated class for the DaftarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-daftar',
  templateUrl: 'daftar.html',
})
export class DaftarPage {

  nama: String = "";
  email: String = "";
  nope: Number = null;
  username: String = "";
  password: String = "";
  password2: String = "";
  alamat: String = "";
  jk: String = "";


  constructor(private event: Events, public loading: LoadingController, public http: Http, public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DaftarPage');
  }

  daftar(){
    if (this.nama == "" || this.email == "" || this.nope == null || this.username == "" || this.password == "" || this.password2 == "" || this.alamat == "" || this.jk == "") {
      const alert = this.alertCtrl.create({
        title: "Peringatan!",
        subTitle: "Biodata harus diisi lengkap",
        buttons: ['Ok']
      });
      alert.present();
    }else{
      if (this.password != this.password2) {
        const alert = this.alertCtrl.create({
          title: "Pemberitahuan",
          subTitle: "Password anda tidak sama",
          buttons: ['Ok']
        });
        alert.present();
      }else{

        let loding = this.loading.create({
          content: "Harap Tunggu..."
        });
    
        loding.present();

        console.log("daftar......");
        let url = "http://japfafoundation.org/public/regis/add_api";

        let header = new Headers({
          'Content-Type': 'application/x-www-form-urlencoded'
        });

        let options = new RequestOptions({ headers: header });

        let body = "nama="+this.nama+"&alamat="+this.alamat+"&jk="+this.jk+"&telp="+this.nope+"&pass="+this.password+"&email="+this.email;

        this.http.post(url, body, options).map(res=>res.json()).subscribe( data => {
          
          if (data == "Sukses") {
            let bio = {
              email: this.email,
              password: this.password
            }
            loding.dismiss();
            this.event.publish("bio_daftar", bio);
            this.navCtrl.pop()
          }
          
        }, err => {
          console.log(err);
          loding.dismiss();
          
        })
        
      }
    }
    
  }

  loginPage(){
    this.navCtrl.pop();
  }

}
