import {
  Component
} from '@angular/core';
import {
  LoadingController,
  NavController,
  NavParams,
  Events
} from 'ionic-angular';
import {
  Storage
} from '@ionic/storage';
import {
  Http
} from '@angular/http';
import { SocialSharing } from "@ionic-native/social-sharing";
/**
 * Generated class for the SmadetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-smadetail',
  templateUrl: 'smadetail.html',
})
export class SmadetailPage {

  prosesnya: any;
  gaugeType = "arch";
  gaugeValue: Number;
  gaugeLabel = "Progress";
  gaugeAppendText = "%";
  tik = 10;

  thresholdConfig = {
    '0': {
      color: 'red'
    },
    '40': {
      color: 'orange'
    },
    '75.5': {
      color: 'green'
    }
  };

  url_base: String = 'https://jfapps.japfafoundation.org/';
  url_share: string = "https://japfafoundation.org/"
  url_gambar: String = "http://jfapps.japfafoundation.org/assets/img/gizi/000001.jpg";
  tipe_gambar: String;
  tampung_laporan: any;
  tampung_gambar_tanggal: any = [];
  tampung_chard: any;
  tabnya: String;
  judul: string = "";
  no: String = "";
  gambar: String = "";
  location: String = "";
  bahasa: string;
  pobjek: String = "";
  implement: string = "";
  sdate: String = "";
  edate: String = "";
  board: Boolean;
  kategori: String = "";
  data_terakhirGR: string;
  constructor(public soialShare: SocialSharing, public storage: Storage, public http: Http, public loading: LoadingController, public event: Events, public navCtrl: NavController, public navParams: NavParams) {
    this.storage.get("info_user").then(val => {
      if (val.type_user == "board") {
        this.tabnya = "info";
        this.board = false;
      } else {
        this.tabnya = "info";
        this.board = true;
      }
    });
    let a = this.navParams.get("parameternya");
    console.log(a);
    this.gaugeValue = 0;
    this.judul = a[0].nama_edudetnya;
    this.gambar = a[0].gambarnya;
    this.no = a[0].id_detedunya;
    this.location = a[0].alamatnya;
    this.pobjek = a[0].tujuannya;
    this.implement = a[0].pelaksananya; 
    this.sdate = a[0].tgl_mulainya;
    this.edate = a[0].tgl_akhirnya;
    this.kategori = a[0].kategori;
  }

  shareSos(){
    this.soialShare.share(this.judul, this.implement,null, this.url_share).then(val => {
      console.log(val);
      
    }).catch(e => {
      console.log(e);
      
    })
  }

  ionViewDidLoad() {
    console.log(this.kategori);
    console.log(this.gambar);
    

    let loding = this.loading.create({
      content: "Harap Tunggu..."
    });

    this.http.get(this.url_base + 'Gambar/view_file_all/' + this.no + '/' + this.kategori)
      .map(respon => respon.json())
      .subscribe((val) => {
        console.log(val);
        this.tampung_laporan = val;

      });

    this.http.get(this.url_base + 'Gambar/view_img_row_tanggal/' + this.no + '/' + this.kategori)
      .map(respon => respon.json())
      .subscribe((val) => {
        loding.dismiss();
        console.log(val);
        this.tampung_gambar_tanggal = val
        console.log(this.tampung_gambar_tanggal);
        
      });

    
    
    if (this.kategori == "nutrition") {

      this.tipe_gambar = "nut_det/";

      this.http.get(this.url_base + 'detgizi/coba_data_proses?id=' + this.no)
      .map(respon => respon.json())
      .subscribe((datanya) => {
        if (datanya.length != 0) {
          this.prosesnya = datanya;
        }
      });

      this.http.get(this.url_base + 'detgizi/coba_chard?id=' + this.no)
      .map(respon => respon.json())
      .subscribe((datanya) => {
        if (datanya.length != 0) {
          this.gaugeValue = datanya[0].progress;
        }
      });
      
    }else if(this.kategori == "education"){

      this.tipe_gambar = "edu_det/";

      this.http.get(this.url_base + 'detedu/coba_chard?id=' + this.no)
      .map(respon => respon.json())
      .subscribe((datanya) => {
        if (datanya.length != 0) {
          this.gaugeValue = datanya[0].progress;
        }
      });

      this.http.get(this.url_base + 'detedu/coba_data_proses?id=' + this.no)
      .map(respon => respon.json())
      .subscribe((datanya) => {
        if (datanya.length != 0) {
          this.prosesnya = datanya;
        }
      });
    }else if(this.kategori == "sport"){

      this.tipe_gambar = "sport_det/";

      this.http.get(this.url_base + 'detsport/coba_chard?id=' + this.no)
      .map(respon => respon.json())
      .subscribe((datanya) => {
        if (datanya.length != 0) {
          this.gaugeValue = datanya[0].progress;
        }
      });

      this.http.get(this.url_base + 'detsport/coba_data_proses?id=' + this.no)
      .map(respon => respon.json())
      .subscribe((datanya) => {
        if (datanya.length != 0) {
          this.prosesnya = datanya;
        }
      });
    }else if(this.kategori == "cosi"){

      this.tipe_gambar = "disas_det/"

      this.http.get(this.url_base + 'detdisas/coba_chard?id=' + this.no)
      .map(respon => respon.json())
      .subscribe((datanya) => {
        if (datanya.length != 0) {
          this.gaugeValue = datanya[0].progress;
        }
      });

      this.http.get(this.url_base + 'detdisas/coba_data_proses?id=' + this.no)
      .map(respon => respon.json())
      .subscribe((datanya) => {
        if (datanya.length != 0) {
          this.prosesnya = datanya;
        }
      });
    }else if(this.kategori == "bencana"){

      this.tipe_gambar = "bencana_det/"

      this.http.get(this.url_base + 'detbencana/coba_chard?id=' + this.no)
      .map(respon => respon.json())
      .subscribe((datanya) => {
        if (datanya.length != 0) {
          this.gaugeValue = datanya[0].progress;
        }
      });

      this.http.get(this.url_base + 'detbencana/coba_data_proses?id=' + this.no)
      .map(respon => respon.json())
      .subscribe((datanya) => {
        if (datanya.length != 0) {
          this.prosesnya = datanya;
        }
      });
    }

    

  }



}
