import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { KotaPage } from '../kota/kota';
import { Storage } from '@ionic/storage';
import { BencanadetailPage } from '../bencanadetail/bencanadetail';
import { EdukatPage } from '../edukat/edukat';

/**
 * Generated class for the BencanaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-bencana',
  templateUrl: 'bencana.html',
})
export class BencanaPage {

  disas: any;
  bahasa: string;
  url_base: String = 'https://jfapps.japfafoundation.org/';
  crl_galeri : String = 'https://jfapps.japfafoundation.org/assets/img/galeri';

  constructor(private storage: Storage, public http: Http, public loading: LoadingController,public navCtrl: NavController, public navParams: NavParams) {
    this.storage.get("bahasa").then(val => {
      this.bahasa = val
    });
  }

  ionViewDidLoad() {
    let loding = this.loading.create({
      content: "Harap Tunggu..."
    });

    loding.present();
    this.http.get(this.url_base+'bencana/mobile_view')
    .map(respon => respon.json())
      .subscribe((val) => {
          loding.dismiss();
          console.log(val);
          this.disas = val;
      });
  }

  kota(id_bencana, nama){
    console.log(id_bencana);
    this.navCtrl.push(EdukatPage, {idnya: id_bencana, judul: nama, kat: "disas"});
  }

}
