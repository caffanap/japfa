import {
  Component
} from '@angular/core';
import {
  NavController
} from 'ionic-angular';
import {
  Storage
} from '@ionic/storage';

@Component({
  selector: 'page-profile-five',
  templateUrl: 'profile-five.html',
})
export class ProfileFivePage {

  user = {};
  crl_galeri : String = 'http://japfafoundation.org/public/assets/img/user/';
  

  constructor(public storage: Storage, public navCtrl: NavController) {
    this.storage.get("info_user").then(val => {
      this.user = {

        name: val.nama_user,
        profileImage: '../assets/img/avatar/girl-avatar.png',
        coverImage: '../assets/img/background/background_hitam.png',
        occupation: val.type_user,
        bergabung: 'Bergabung pada '+val.tgl_daftar,
        tempat: val.tempat,
        deskripsi: val.deskripsi,
        email: val.email, 
        gambar: val.gambar,
        alamat: val.alamat,
        notelp: val.no_telf
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfileFivePage');
  }

}
