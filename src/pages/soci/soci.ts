import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { KotaPage } from '../kota/kota';
import { Storage } from '@ionic/storage';
import { EducationPage } from '../education/education';
import { EdukatPage } from '../edukat/edukat';

/**
 * Generated class for the SociPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-soci',
  templateUrl: 'soci.html',
})
export class SociPage {

  disas: any;
  bahasa: string;
  url_base: String = 'https://jfapps.japfafoundation.org/';
  crl_galeri : String = 'https://jfapps.japfafoundation.org/assets/img/galeri';

  constructor(private storage: Storage, public http: Http, public loading: LoadingController,public navCtrl: NavController, public navParams: NavParams) {
    this.storage.get("bahasa").then(val => {
      this.bahasa = val
    });
  }

  ionViewDidLoad() {
    let loding = this.loading.create({
      content: "Harap Tunggu..."
    });

    loding.present();
    this.http.get(this.url_base+'disas/mobile_view')
    .map(respon => respon.json())
      .subscribe((val) => {
          loding.dismiss();
          console.log(val);
          this.disas = val;
      });
  }

  kota(id_disas, nama){
    console.log(id_disas);
    this.navCtrl.push(EdukatPage, {idnya: id_disas, judul: nama, kat: "soci"});
  }

}
