import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { SmadetailPage } from '../smadetail/smadetail';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the SdevelopPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-sdevelop',
  templateUrl: 'sdevelop.html',
})
export class SdevelopPage {


  judul: String = "";
  url_base: String = 'https://jfapps.japfafoundation.org/';
  crl_galeri : String = 'https://jfapps.japfafoundation.org/assets/img/galeri';
  tampung_education_awal: any;
  tampung_education: any;
  id: Number;
  bahasa: string = "";
  nama_edudet: string = "";
  pelaksana: string = "";
  tujuan: string = "";

  constructor(public storage: Storage, public http: Http, public loading: LoadingController,public navCtrl: NavController, public navParams: NavParams) {
    this.id = this.navParams.get("idnya");
    this.judul = this.navParams.get("judul");
    
    this.storage.get("bahasa").then(val => {
      this.bahasa = val
    });
    this.initMulai();
  }
 
  initMulai() {

    let loding = this.loading.create({
      content: "Harap Tunggu..."
    });

    loding.present();
    this.http.get(this.url_base+'detedu/mobile_view_atas?id='+this.id)
    .map(respon => respon.json())
      .subscribe((val) => {
          console.log(val);
          this.tampung_education_awal = val;

      });

        this.http.get(this.url_base+'detedu/mobile_view?id='+this.id)
        .map(respon => respon.json())
          .subscribe((val) => {
            loding.dismiss();
              console.log(val);
              this.tampung_education = val;
    
          });
        
      
     

  }

  gdvocational(id_edu, id_detedu, lat, long, nama_edudet_indo,nama_edudet_ing, pelaksana_indo, pelaksana_ing, status, tgl_akhir, tgl_mulai, tgl_reg, tujuan_indo, tujuan_ing, gambar, alamat){
    
    
    this.storage.get("bahasa").then(val => {
      if (val == "ing") {
        this.nama_edudet = nama_edudet_ing
        this.pelaksana = pelaksana_ing
        this.tujuan = tujuan_ing
        console.log(this.nama_edudet, this.pelaksana, this.tujuan);
        
      }else if(val == "indo"){
        this.nama_edudet = nama_edudet_indo
        this.pelaksana = pelaksana_indo
        this.tujuan = tujuan_indo
      }


    let data  = [
      {
        id_edunya: id_edu,
        id_detedunya: id_detedu,
        latnya: lat,
        longnya: long,
        nama_edudetnya: this.nama_edudet,
        pelaksananya: this.pelaksana,
        statusnya: status,
        tgl_akhirnya: tgl_akhir,
        tgl_mulainya: tgl_mulai,
        tgl_regnya: tgl_reg,
        tujuannya: this.tujuan,
        gambarnya: gambar,
        alamatnya: alamat,
        kategori: "education"
      }
    ];
    this.storage.set("data_detail", data);
    this.navCtrl.push(SmadetailPage,{ parameternya: data });




    })
    

  }

}
