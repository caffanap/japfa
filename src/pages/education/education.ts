import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { SdevelopPage } from '../sdevelop/sdevelop';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { EdukatPage } from '../edukat/edukat';

/**
 * Generated class for the EducationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-education',
  templateUrl: 'education.html',
})
export class EducationPage {
  
  bahasa: string;
  url_base: String = 'https://jfapps.japfafoundation.org/';
  crl_galeri : String = 'https://jfapps.japfafoundation.org/assets/img/galeri';
  dataList: any;
  constructor(public storage: Storage, public alertCtrl: AlertController, public loading: LoadingController,  public http: Http, public navCtrl: NavController, public navParams: NavParams) {
    this.storage.get("bahasa").then(val => {
      this.bahasa = val
    })
  }

  ionViewDidLoad() {
    let loding = this.loading.create({
      content: "Harap Tunggu..."
    });

    loding.present();
    this.http.get(this.url_base+'edu/mobile_view')
    .map(respon => respon.json())
      .subscribe((val) => {
        loding.dismiss();
          console.log(val);
          this.dataList = val;

      });
  }

  sdevelop(id, nama){ 
    console.log(id);
    
    this.navCtrl.push(EdukatPage, {idnya: id, judul: nama, kat: "edu"});
  }

}
