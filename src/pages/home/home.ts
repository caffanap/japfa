import {
  Component,
  ViewChild
} from '@angular/core';
import {
  NavController,
  Slides,
  MenuController,
  AlertController,
  LoadingController,
  ActionSheetController
} from 'ionic-angular';
import {
  SafeResourceUrl,
  DomSanitizer
} from "@angular/platform-browser";
import { ProfilePage } from '../profile/profile';
import { EducationPage } from '../education/education';
import { Storage } from '@ionic/storage';
import { GdvocationalPage } from '../gdvocational/gdvocational';
import { SportPage } from '../sport/sport';
import { SociPage } from '../soci/soci';

import { Http } from '@angular/http';
import { SocialSharing } from '@ionic-native/social-sharing';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { KirimemailPage } from '../kirimemail/kirimemail';
import { BencanaPage } from '../bencana/bencana';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild('slider') slider: Slides;

  backgrounds = [
    'assets/img/kartu/bg_home.png',
    'assets/img/kartu/inzi_page.png',
    'assets/img/kartu/lbr_page.jpg'
  ];

  vid: string = "https://www.youtube.com/embed/hHYDVmWE9FI";

  url_base : String = 'https://jfapps.japfafoundation.org/';
  tampung_video : any;
  tampung_news : any;
  bahasa : string;

  trustedVideoUrl: SafeResourceUrl;
  constructor(private iab: InAppBrowser,public sharing: SocialSharing, public actionSheetCtrl: ActionSheetController, public http: Http, public loading: LoadingController,public alertCtrl: AlertController, public menuCtrl: MenuController, public storage: Storage, public navCtrl: NavController, private dom: DomSanitizer) {
    this.menuCtrl.enable(true);
  }

  //ionViewWillEnter(): void {
  //  this.trustedVideoUrl = this.dom.bypassSecurityTrustResourceUrl(this.vid);
  //}

  changeBahasa(){

    const alert = this.alertCtrl.create({
      title: "Pilih Bahasa",
      inputs: [{
        type: 'radio',
        label: 'Indonesia',
        value: 'indo'
      },{
        type: 'radio',
        label: 'Inggris',
        value: 'ing'
      }],
      buttons: [
        {
          text: "OK",
          handler: data => {
            console.log(data);
            this.bahasa = data;
            this.storage.set("bahasa", data);
          }
        }
      ]
    }).present();

  }

  pilihPutarShare(viddeoLink, title){

    let act = this.actionSheetCtrl.create({
      title: "Pilih tindakan",
      buttons: [
        {
          icon: "play",
          handler: () => {
            //window.location = viddeoLink
            const browser = this.iab.create(viddeoLink);
            browser.show()
          },
          text: "Putar Video",
        },
        {
          icon: "share",
          handler: () => {
            console.log("bagian");
            this.sharing.share(title, null, null, viddeoLink).then(val => {
              console.log(val);
              
            }).catch(e => {
              console.log(e);
              
            })
          },
          text: "Bagikan Video"
        }
      ]
    }).present()
  }

  profile(){
    this.navCtrl.push(ProfilePage);
  }

  education(){
    this.navCtrl.push(EducationPage);
  }

  nutrition(){
    this.navCtrl.push(GdvocationalPage);
  }

  sport(){
    this.navCtrl.push(SportPage);
  }

  socinya(){
    this.navCtrl.push(SociPage);
  }

  bencana(){
    this.navCtrl.push(BencanaPage);
  }

  ionViewDidLoad(){

    this.storage.get("bahasa").then(val => {
      if (val == "indo") {
        this.bahasa = val;
      }else if (val == "ing") {
        this.bahasa = val;
      }
    });

    let loding = this.loading.create({
      content: "Harap Tunggu..."
    });
    //YOUTUBE
    this.http.get(this.url_base+'video/mobile_view')
      .map(respon => respon.json())
        .subscribe((val) => {
          loding.dismiss();
            console.log(val);
            this.tampung_video = val;
  
        });

    //NEWS
    this.http.get(this.url_base+'news/mobile_view')
      .map(respon => respon.json())
        .subscribe((val) => {
          loding.dismiss();
            console.log(val);
            this.tampung_news = val;
  
        });
  }

}
