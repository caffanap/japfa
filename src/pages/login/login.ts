import {
  Component
} from '@angular/core';
import {
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  MenuController,
  Events
} from 'ionic-angular';
import {
  HomePage
} from '../home/home';
import {
  Http,
  RequestOptions,
  Headers
} from '@angular/http';
import {
  Storage
} from '@ionic/storage';
import { DaftarPage } from '../daftar/daftar';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  username: String = "";
  password: String = "";
  url_base: String = 'https://japfafoundation.org/public/';
  crl_galeri: String = 'https://japfafoundation.org/public/assets/img/galeri';

  constructor(private event: Events, public menuCtrl: MenuController, public alertCtrl: AlertController, public storage: Storage, public loading: LoadingController, public http: Http, public navCtrl: NavController, public navParams: NavParams) {
    this.menuCtrl.enable(false);
    this.event.subscribe("bio_daftar", (data) =>{
      
      this.username = data.email;
      this.password = data.password;
      this.masuk();
      
    });
  }

  registerPage(){
    this.navCtrl.push(DaftarPage);
  }

  masuk() {

    let loding = this.loading.create({
      content: "Harap Tunggu..."
    });

    loding.present();

    this.http.get(this.url_base + 'login/login_member/' + this.username + '/' + this.password)
      .map(respon => respon.json())
      .subscribe((val) => {
        loding.dismiss();
        if (val.status == "N") {
          const alert = this.alertCtrl.create({
            title: "Pemberitahuan",
            subTitle: "Email atau Password Anda Salah!",
            buttons: ['OK']
          });
          alert.present();
        } else {
          if (val[0].status == "N") {
            const alert = this.alertCtrl.create({
              title: "Pemberitahuan",
              subTitle: "Akun anda telah di non-aktifkan silakan hubungi admin",
              buttons: ['OK']
            });
            alert.present();
          } else {
            val.forEach(data => {
              this.storage.set("info_user", data);
              this.storage.set("bahasa", "indo");
              this.event.publish('nama', data.nama_user);
            });
            this.navCtrl.setRoot(HomePage);
          }
        }

      });

    //
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
