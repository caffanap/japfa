import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { SmadetailPage } from '../smadetail/smadetail';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the KotaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-kota',
  templateUrl: 'kota.html',
})
export class KotaPage {

  url_base: String = 'https://jfapps.japfafoundation.org/';
  crl_galeri : String = 'https://jfapps.japfafoundation.org/assets/img/galeri';
  judul: String = "";
  id: Number;
  tampung_disas_awal: any;
  tampung_disas: any;
  nama_disasdet: string;
  pelaksana: string;
  tujuan: string;
  bahasa: string;

  constructor(public storage: Storage,public http: Http, public loading: LoadingController,public navCtrl: NavController, public navParams: NavParams) {
    this.id = this.navParams.get("idnya");
    this.judul = this.navParams.get("judul");
    this.storage.get("bahasa").then(val => {
      this.bahasa = val
    });
  }

  ionViewDidLoad() {
    let loding = this.loading.create({
      content: "Harap Tunggu..."
    });

    loding.present();
    this.http.get(this.url_base+'detdisas/mobile_view_atas?id='+this.id)
    .map(respon => respon.json())
      .subscribe((val) => {
          console.log(val);
          this.tampung_disas_awal = val;

      });

      this.storage.get("bahasa").then(val => {
        this.http.get(this.url_base+'detdisas/mobile_view?id='+this.id+'&bahasa='+val)
        .map(respon => respon.json())
          .subscribe((val) => {
            loding.dismiss();
              console.log(val);
              this.tampung_disas = val;
    
          });
      });
  }


  smadetel(id_disas, id_detdisas, lat, long, nama_disasdet_indo,nama_disasdet_ing, pelaksana_indo,pelaksana_ing, status, tgl_akhir, tgl_mulai, tgl_reg, tujuan_indo,tujuan_ing, gambar, alamat){
    
    this.storage.get("bahasa").then(val => {
      if (val == "ing") {
        this.pelaksana = pelaksana_ing
        this.tujuan = tujuan_ing
        this.nama_disasdet = nama_disasdet_ing
      }else if (val == "indo") {
        this.pelaksana = pelaksana_indo
        this.tujuan = tujuan_indo
        this.nama_disasdet = nama_disasdet_indo
      }

      let data  = [
      {
        id_edunya: id_disas,
        id_detedunya: id_detdisas,
        latnya: lat,
        longnya: long,
        nama_edudetnya: this.nama_disasdet,
        pelaksananya: this.pelaksana,
        statusnya: status,
        tgl_akhirnya: tgl_akhir,
        tgl_mulainya: tgl_mulai,
        tgl_regnya: tgl_reg,
        tujuannya: this.tujuan,
        gambarnya: gambar,
        alamatnya: alamat,
        kategori: "cosi"
      }
    ];
    this.storage.set("data_detail", data);
    this.navCtrl.push(SmadetailPage,{ parameternya: data });

    });
    
    

  }


}
