import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { SmadetailPage } from '../smadetail/smadetail';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { EdukatPage } from '../edukat/edukat';
 
/**
 * Generated class for the GdvocationalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */ 
@Component({
  selector: 'page-gdvocational',
  templateUrl: 'gdvocational.html',
})
export class GdvocationalPage {
  bahasa: string = "";
  nutrition: any;
  nama_nutdet:string;
  tujuan: string;
  pelaksana: string;
  url_base: String = 'https://jfapps.japfafoundation.org/';
  crl_galeri : String = 'https://jfapps.japfafoundation.org/assets/img/galeri';

  constructor(public storage: Storage, public http: Http, public loading: LoadingController, public navCtrl: NavController, public navParams: NavParams) {
    this.storage.get("bahasa").then(val => {
      this.bahasa = val
    })
  }

  ionViewDidLoad() {
    
    let loding = this.loading.create({
      content: "Harap Tunggu..."
    }); 
 
    loding.present();
    this.http.get(this.url_base+'gizi/mobile_view')
    .map(respon => respon.json())
      .subscribe((val) => {
          loding.dismiss();
          console.log(val);
          this.nutrition = val;

      });
    
  }

  kota(id_nut, nama){
    console.log(id_nut);
    this.navCtrl.push(EdukatPage, {idnya: id_nut, judul: nama, kat: "nut"});
  }

  // smadetail(id){
  //   console.log(id);
  //   this.http.get(this.url_base+'detgizi/mobile_view_atas?id='+id+'&bahasa='+this.bahasa)
  //   .map(respon => respon.json())
  //     .subscribe((val) => { 
            
  //           this.storage.get("bahasa").then(res => {
  //             if (res == "ing") {
  //               this.nama_nutdet = val[0].nama_nutdet_ing
  //               this.pelaksana = val[0].pelaksana_ing
  //               this.tujuan = val[0].tujuan_ing
  //             }else if (res == "indo") {
  //               this.nama_nutdet = val[0].nama_nutdet
  //               this.pelaksana = val[0].pelaksana
  //               this.tujuan = val[0].tujuan
  //             }

  //             let dataparam = [{
  //               id_edunya: val[0].id_nut,
  //               id_detedunya: val[0].id_detnut,
  //               latnya: val[0].lat,
  //               longnya: val[0].long,
  //               nama_edudetnya: this.nama_nutdet,
  //               pelaksananya: this.pelaksana,
  //               statusnya: val[0].status,
  //               tgl_akhirnya: val[0].tgl_akhir,
  //               tgl_mulainya: val[0].tgl_mulai,
  //               tgl_regnya: val[0].tgl_reg,
  //               tujuannya: this.tujuan,
  //               gambarnya: val[0].gambar,
  //               alamatnya: val[0].alamat,
  //               kategori: "nutrition"
  //             }];

  //             this.storage.set("data_detail", dataparam);
  //             console.log(dataparam);
  //             this.navCtrl.push(SmadetailPage, {parameternya: dataparam});
  //           });

            

 
  //     });
      
  // }

}
