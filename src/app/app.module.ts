import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from "../pages/login/login";
import { ProfilePage } from "../pages/profile/profile";
import { EducationPage } from "../pages/education/education";
import { SdevelopPage } from "../pages/sdevelop/sdevelop";
import { GdvocationalPage } from "../pages/gdvocational/gdvocational";
import { SmadetailPage } from "../pages/smadetail/smadetail";
import { SportPage } from "../pages/sport/sport";
import { SociPage } from "../pages/soci/soci";
import { ProfileFivePage } from "../pages/profile-five/profile-five";
import { HttpModule } from "@angular/http"; 

import { NgxGaugeModule } from "ngx-gauge";

import { SocialSharing } from "@ionic-native/social-sharing";

import "rxjs/add/operator/map";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { MatCardModule, MatGridListModule, MatRippleModule, MatButtonModule, MatSelectModule } from "@angular/material";
import { IonicStorageModule } from "@ionic/storage";
import { KotaPage } from '../pages/kota/kota';
import { IonicImageViewerModule } from "ionic-img-viewer";
import { MailPage } from '../pages/mail/mail';
import { DetailmailPage } from '../pages/detailmail/detailmail';
import { KirimemailPage } from '../pages/kirimemail/kirimemail';
import { RlTagInputModule } from "angular2-tag-input";
import { DaftarPage } from '../pages/daftar/daftar';
import { BencanaPage } from '../pages/bencana/bencana';
import { BencanadetailPage } from '../pages/bencanadetail/bencanadetail';
import { EdukatPage } from '../pages/edukat/edukat';
import { NutdetPage } from '../pages/nutdet/nutdet';
import { SportdetPage } from '../pages/sportdet/sportdet';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    EdukatPage,
    BencanaPage,
    BencanadetailPage,
    DaftarPage,
    ProfilePage,
    EducationPage,
    SdevelopPage,
    GdvocationalPage,
    SmadetailPage,
    SportPage,
    NutdetPage,
    SportdetPage,
    SociPage,
    KotaPage,
    MailPage,
    DetailmailPage,
    KirimemailPage,
    ProfileFivePage
  ], 
  imports: [
    BrowserModule,
    HttpModule,
    IonicImageViewerModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp, { mode: 'md' }),
    NgxGaugeModule,
    RlTagInputModule,
    MatButtonModule,
    MatSelectModule,
    MatRippleModule,
    MatGridListModule,
    MatCardModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    BencanaPage,
    BencanadetailPage,
    EdukatPage,
    NutdetPage,
    SportdetPage,
    DaftarPage,
    LoginPage, 
    ProfilePage,
    EducationPage,
    SdevelopPage,
    GdvocationalPage,
    SmadetailPage,
    SportPage,
    SociPage,
    MailPage,
    KotaPage,
    DetailmailPage,
    KirimemailPage,
    ProfileFivePage
  ],
  providers: [
    StatusBar,
    SocialSharing,
    InAppBrowser,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
