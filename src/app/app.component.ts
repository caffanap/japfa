import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { Storage } from '@ionic/storage';
import { ProfileFivePage } from '../pages/profile-five/profile-five';
import { KirimemailPage } from '../pages/kirimemail/kirimemail';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  public rootPage;

  pages: Array<{title: string, component: any}>;

  nama: String = "";

  constructor(public alertCtrl: AlertController, public event: Events,public storage: Storage, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    

    this.event.subscribe('nama', (data) => {
      this.gantinama(data);
    });

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.backgroundColorByHexString('#cc480a');
      this.splashScreen.hide();
      this.storage.get("info_user").then(val => {
        this.rootPage = val ? HomePage : LoginPage;
        this.nama = val ? val.nama_user : "";
      });
    });
  }

  gantinama(data){
    this.nama = data;
  }

  keluar(){
    this.storage.remove("info_user");
    this.nav.setRoot(LoginPage);
  }

  home(){
    this.nav.setRoot(HomePage);
  }
  
  email(){
    this.nav.setRoot(KirimemailPage);
  }

  profilenya(){
    this.nav.setRoot(ProfileFivePage);
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
